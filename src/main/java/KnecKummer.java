import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import commands.BlackListCommand;
import commands.GitCommand;
import commands.WhiteListCommand;
import entities.AnonDiscordUser;
import listeners.PrivateMessageListener;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import config.KnecKummerConfig;
import net.dv8tion.jda.api.OnlineStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public class KnecKummer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KnecKummer.class);
    //Builds and connects the bot
    public static void main(String[] args) throws LoginException {
        LOGGER.info("Starting KnecKummer..");
        //Connection between anon user and discord user, needed to get a pseudo anon id
        HashMap<Long, AnonDiscordUser> discordUserToAnon = new HashMap<>();
        // define an eventwaiter, dont forget to add this to the JDABuilder!
        EventWaiter waiter = new EventWaiter();
        CommandClientBuilder client = new CommandClientBuilder();
        PrivateMessageListener privateMessageListener = new PrivateMessageListener(discordUserToAnon);
        client.setPrefix(KnecKummerConfig.get().botPrefix());
        client.setOwnerId(KnecKummerConfig.get().ownerid());
        client.addCommands(
                // add commands here
                new BlackListCommand(discordUserToAnon),
                new WhiteListCommand(discordUserToAnon),
                new GitCommand()
        );
        // start getting a bot account set up
        JDA jda = new JDABuilder(AccountType.BOT)
                // set the token
                .setToken(KnecKummerConfig.get().discordToken())
                // set the game for when the bot is loading
                .setStatus(OnlineStatus.ONLINE)
                // add the listeners
                .addEventListeners(waiter, privateMessageListener, client.build())
                // start it up!
                .build();
        //We need a running JDA instance, so just set it after its build
        privateMessageListener.setJda(jda);
        //Schedule a timer to clear the map every night at 2am
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 2);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        Timer timer = new Timer();
        timer.schedule(new clearUserList(discordUserToAnon), today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // period: 1 day

        LOGGER.info("KnecKummer started and ready for QQ");
    }
}
