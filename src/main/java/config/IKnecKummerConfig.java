package config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
@Sources({"file:Config/Config.properties"})
public interface IKnecKummerConfig extends Config{
    @Key("login.discordToken")
    String discordToken();

    @Key("bot.prefix")
    String botPrefix();

    @Key("bot.ownerid")
    String ownerid();

    @Key("bot.channelToPost")
    Long channelToPost();

    @Key("bot.greetingmessage")
    String greetingMessage();

    @Key("discord.modrole")
    Long modRole();

    @Key("bot.blacklistMessage")
    String blacklistMessage();

}
