package config;

import org.aeonbits.owner.ConfigFactory;

public final class KnecKummerConfig {

    private static KnecKummerConfig instance = null;

    private static IKnecKummerConfig config = ConfigFactory.create(IKnecKummerConfig.class);

    private KnecKummerConfig() {
    }

    public static IKnecKummerConfig get() {
        if (instance == null) {
            instance = new KnecKummerConfig();
        }
        return config;
    }
}
