package entities;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class AnonDiscordUser {
    private String name;
    private Long discordUserId;
    //Default is false
    private Boolean blacklisted;
    private Color color;

    /**
     * Construct a new anon user, creating a new UUID.
     * @param discordUserId The discord user associated with the UUID
     */
    public AnonDiscordUser(Long discordUserId) {

        this.discordUserId = discordUserId;
        this.blacklisted = false;
        this.color = new Color((int)(Math.random() * 0x1000000));
        Map<String,Integer> map = new HashMap<String,Integer>();
        String tName = "";
        for(int i = 0; i < 3; ++i)
        {
            String s = null;
            try {
                s = choose(new File("Config/dictionary.txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if(!map.containsKey(s))
                map.put(s, 0);
            map.put(s, map.get(s) + 1);
        }
        for (String nameM : map.keySet()) {
            nameM = nameM.substring(0,1).toUpperCase() + nameM.substring(1).toLowerCase();
            tName += nameM;
        }
        this.name = tName;
    }

    public static String choose(File f) throws FileNotFoundException
    {
        String result = null;
        Random rand = new Random();
        int n = 0;
        for(Scanner sc = new Scanner(f); sc.hasNext(); )
        {
            ++n;
            String line = sc.nextLine();
            if(rand.nextInt(n) == 0)
                result = line;
        }

        return result;
    }


    public Boolean getBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(Boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    public Long getDiscordUserId() {
        return discordUserId;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
