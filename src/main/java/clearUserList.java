import entities.AnonDiscordUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.TimerTask;

public class clearUserList extends TimerTask {
    private HashMap<Long, AnonDiscordUser> discordUserHashMap;
    private static final Logger LOGGER = LoggerFactory.getLogger(clearUserList.class);

    public clearUserList(HashMap<Long, AnonDiscordUser> discordUserToAnon) {
        this.discordUserHashMap = discordUserToAnon;
    }

    @Override
    public void run() {
        LOGGER.info("Clearing the hashmap");
        discordUserHashMap.clear();
    }
}
