package listeners;

import config.KnecKummerConfig;
import entities.AnonDiscordUser;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.HashMap;

/**
 * Listens for dms and mirrors them into the channel specified in the config file.
 */
public class PrivateMessageListener extends ListenerAdapter {
    private JDA jda;
    private final HashMap<Long, AnonDiscordUser> discordUserToAnon;

    /**
     * Create a new listen handler,
     * @param discordUserToAnon
     */
    public PrivateMessageListener(HashMap<Long, AnonDiscordUser> discordUserToAnon) {
        this.discordUserToAnon = discordUserToAnon;
    }

    private void sendDiscordMessage(TextChannel channel, String message, AnonDiscordUser anonDiscordUser){
        EmbedBuilder bobTheEmbeddBuilder = new EmbedBuilder();
        bobTheEmbeddBuilder.setColor(anonDiscordUser.getColor());
        bobTheEmbeddBuilder.setTitle(anonDiscordUser.getName());
        bobTheEmbeddBuilder.addField("Sagte: ", message, false);
        channel.sendMessage(bobTheEmbeddBuilder.build()).queue();
    }

    /**
     * Called on each private message.
     * @param event The message event
     */
    public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent event) {
        //(duh)
        if(event.getAuthor().isBot())
            return;
        Long userId = event.getAuthor().getIdLong();
        AnonDiscordUser anonDiscordUser = null;
        //New User register him
        if(!discordUserToAnon.containsKey(userId)){
            anonDiscordUser = new AnonDiscordUser(userId);
            discordUserToAnon.put(userId, anonDiscordUser);
            event.getChannel().sendMessage(KnecKummerConfig.get().greetingMessage()).queue();
            //reoccuring user, get id
        }else{
            anonDiscordUser = discordUserToAnon.get(userId);
        }
        //Is that dude blacklisted?
        if(!anonDiscordUser.getBlacklisted()) {
            TextChannel t = jda.getTextChannelById(Long.valueOf(KnecKummerConfig.get().channelToPost()));

            String kummerMessage = event.getMessage().getContentStripped();
            String messageToSend = null;
            //Length check for embedded messages. Only 1024 chars are allowed
            if(kummerMessage.length() > 1024) {
            do{
                String kummerSplit = kummerMessage.substring(0, 1024);
                int lastSpaceIndex = kummerSplit.lastIndexOf(" ");
                messageToSend = kummerMessage.substring(0, lastSpaceIndex);
                kummerMessage = kummerMessage.substring(lastSpaceIndex, kummerMessage.length());
                sendDiscordMessage(t, messageToSend, anonDiscordUser);

            }while (kummerMessage.length() > 1024);
            }
            sendDiscordMessage(t, kummerMessage, anonDiscordUser);

        } else {
            event.getChannel().sendMessage(KnecKummerConfig.get().blacklistMessage()).queue();
        }
    }

    public void setJda(JDA jda) {
        this.jda = jda;
    }
}
