package commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import config.KnecKummerConfig;
import entities.AnonDiscordUser;
import net.dv8tion.jda.api.entities.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

/**
 * Maybe that dude wasnt a troll, command to allow him post again
 */
public class WhiteListCommand extends Command {
    private static final Logger LOGGER = LoggerFactory.getLogger(WhiteListCommand.class);
    private final Long modRole;
    private final HashMap<Long, AnonDiscordUser> discordUserToAnon;

    /**
     * Construct a new Whitelist command, needs the map of all anon users.
     * @param discordUserToAnon Map discorduser -> anon user
     */
    public WhiteListCommand(HashMap<Long, AnonDiscordUser> discordUserToAnon){
        this.name = "whitelist";
        this.aliases = new String[]{"wl"};
        this.help = "Whitelist a given user, needs the anonymous KummerKasten id as argument";
        this.arguments = "Anonymous Kummerkasten id";
        this.guildOnly = false;
        this.modRole = KnecKummerConfig.get().modRole();
        this.discordUserToAnon = discordUserToAnon;
    }

    /**
     * Whitelist command, checks the permission.
     * @param commandEvent The message from the mod
     */
    @Override
    protected void execute(CommandEvent commandEvent) {
        List<Role> userRoles = commandEvent.getMember().getRoles();
        //Check if user has the permission to do this command
        for(Role r : userRoles){
            if(r.getIdLong() == modRole){
                whiteListUser(commandEvent);
                break;
            }
        }
    }

    /**
     * Whitelists a user so he can post again.
     * @param commandEvent The message from the mod
     */
    private void whiteListUser(CommandEvent commandEvent) {
        //Get the user from our hashmap
        for(AnonDiscordUser a : discordUserToAnon.values()) {
            if(commandEvent.getArgs().split(" ")[0].equals(a.getName())){
                a.setBlacklisted(false);
                discordUserToAnon.replace(a.getDiscordUserId(), a);
                commandEvent.getMessage().addReaction("✅").queue();
            }
        }
    }
}
