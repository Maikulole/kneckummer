package commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import config.KnecKummerConfig;
import entities.AnonDiscordUser;
import net.dv8tion.jda.api.entities.Role;

import java.util.HashMap;
import java.util.List;

/**
 * Command to keep trolls out
 */
public class BlackListCommand extends Command {
    private final Long modRole;
    private final HashMap<Long, AnonDiscordUser> discordUserToAnon;

    /**
     * Construct a new BlackList command, needs the map of all anon users.
     * @param discordUserToAnon Map discorduser -> anon user
     */
    public BlackListCommand(HashMap<Long, AnonDiscordUser> discordUserToAnon){
        this.name = "blacklist";
        this.aliases = new String[]{"bl"};
        this.help = "Blacklist a given user, needs the anonymous KummerKasten id as argument";
        this.arguments = "Anonymous Kummerkasten id";
        this.guildOnly = false;
        this.modRole = Long.valueOf(KnecKummerConfig.get().modRole());
        this.discordUserToAnon = discordUserToAnon;
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        List<Role> userRoles = commandEvent.getMember().getRoles();
        //Check if user has the permission to do this command
        for(Role r : userRoles){
            if(r.getIdLong() == modRole){
                blackListUser(commandEvent);
                break;
            }
        }
    }

    /**
     * Blacklists a user so he can't post anymore.
     * @param commandEvent The message from the mod
     */
    private void blackListUser(CommandEvent commandEvent) {
        //Get the user from our hashmap
        for(AnonDiscordUser a : discordUserToAnon.values()) {
            if(commandEvent.getArgs().split(" ")[0].equals(a.getName())){
                a.setBlacklisted(true);
                discordUserToAnon.replace(a.getDiscordUserId(), a);
                commandEvent.getMessage().addReaction("✅").queue();
            }
        }
    }
}
