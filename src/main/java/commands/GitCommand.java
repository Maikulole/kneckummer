package commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

/**
 * Quick and dirty git command for people interested in trying to improve this superior code.
 */
public class GitCommand extends Command {

    public GitCommand() {
        this.name = "git";
        this.help = "Link to the bots git";
        this.guildOnly = false;
    }
    @Override
    protected void execute(CommandEvent commandEvent) {
        commandEvent.getChannel().sendMessage("https://gitlab.com/Maikulole/kneckummer").queue();
    }
}
