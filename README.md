# KnecKummer  

Discord bot that sends incoming DM's anonymously to a channel. Features black and whitelisting.  

## Config  

Copy paste the Config-Template.properties and fill in your values. No " " needed, just fill in the values.  
    
    //Prefix the bot will be listening for
    bot.prefix=!
    //The ownerid of the bot to have someone responsible for its shit (wont be me)
    bot.ownerid=
    //Message that is send on first contact to a user dm'ing the bot.
    //Will be sent again after the ID resets due to anon stuff
    bot.greetingmessage=
    //ID of the channel where the bot will be posting in
    bot.channelToPost=
    //Message a user gets if he's blacklisted
    bot.blacklistMessage=
    //The role of your mod that is allowed to blacklist people
    discord.modrole=
    //Your discord token
    login.discordToken=  
    
   